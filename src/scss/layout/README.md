# Layout

La carpeta `layout/` contiene todo lo que interviene en el diseño del sitio o la aplicación. Esta carpeta podría tener hojas de estilo para las partes principales del sitio (header, footer, navegación, sidebar...), el sistema de grillas o incluso estilos CSS para todos los formularios.

Referencia: [Sass Guidelines](http://sass-guidelin.es/) > [Architecture](http://sass-guidelin.es/#architecture) > [Layout folder](http://sass-guidelin.es/#layout-folder)