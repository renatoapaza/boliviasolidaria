# Pages

Si tiene estilos específicos de página, es mejor colocarlos en una carpeta `pages/`, en un archivo con el nombre de la página. Por ejemplo, no es raro tener estilos muy específicos para la página de inicio, de ahí la necesidad de un archivo `_home.scss` en` pages/`.

*Nota: Dependiendo de su proceso de deployment, estos archivos se pueden invocar solos para evitar fusionarlos con los demás en la hoja de estilo resultante. Realmente depende de ti.*

Referencia: [Sass Guidelines](http://sass-guidelin.es/) > [Architecture](http://sass-guidelin.es/#architecture) > [Pages folder](http://sass-guidelin.es/#pages-folder)